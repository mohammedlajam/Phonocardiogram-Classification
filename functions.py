# Importing libraries:
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# signal processing and feature extraction libraries:
import librosa
import librosa.display
import IPython.display as ipd
import scipy.fft
from scipy.signal import welch
from PyEMD import EMD
from skimage.restoration import denoise_wavelet
from scipy.signal import filtfilt
import scipy

# Variables:
signal = []  # original signal
imfs = []  # imfs signals
wl_signal = []  # wavelet-denoising signal
filtered_signal = []  # digital-filter signal
emd_wavelet_signal = []  # signal after using EMD and Wavelet-Denoising methods
emd_dfilter_signal = []  # signal after using EMD and Digital-Filter methods
emd_wl_dfilter_signal = []  # signal after using EMD, Wavelet-Denoising and Digital-Filter methods


# 1. VISUALIZATION FUNCTIONS:
# Function to display the audio file based on its index:
def display_audio(file_path, audio_index):
    return ipd.Audio(file_path[audio_index])


# Function to extract the signal and sampling frequency:
def signal_extraction(file_path, audio_index, sampling_rate=''):
    global signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    print(f'Signal: {signal[:10]}')
    print(f'Signal Shape: {signal.shape}')
    print(f'Sample Rate: {sr}')


# Function to visualize the signal in Time-Domain:
def signal_time_domain(file_path, audio_index, sampling_rate='', zoom_1='', zoom_2=''):
    global signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    zoom_1 = 1 if zoom_1 == 0 else zoom_1
    if zoom_1 and zoom_2:
        pd.Series(signal[zoom_1:zoom_2]).plot(figsize=(15, 5), lw=0.5,
                                              title=f'Zoomed Row Audio Signal ({zoom_1} - {zoom_2})')
    else:
        pd.Series(signal).plot(figsize=(15, 5), lw=0.5, title='Complete Row Audio Signal')
    return plt.show()


# Function to display the signal in Frequency-Domain (Spectrum):
def spectrum(file_path, audio_index, sampling_rate='', zoom_1='', zoom_2=''):
    global signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    n_fft = 2048
    ft = np.abs(librosa.stft(signal[:n_fft], hop_length=n_fft+1))
    zoom_1 = 1 if zoom_1 == 0 else zoom_1
    plt.figure(figsize=(15, 5))
    if zoom_1 and zoom_2:
        plt.plot(ft[zoom_1:zoom_2])
        plt.title(f'Zoomed Spectrum ({zoom_1} - {zoom_2})')
    else:
        plt.plot(ft)
        plt.title('Complete Spectrum')
    plt.xlabel('Frequency')
    plt.ylabel('Amplitude')
    return plt.show()


# 2. SIGNAL PROCESSING FUNCTIONS:
# Function to extract IMFs using Empirical Mode Decomposition (EMD):
def empirical_mode_decomposition(file_path, audio_index, sampling_rate='', plot=bool, zoom_1='', zoom_2=''):
    global signal, imfs
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    emd = EMD()
    imfs = emd(signal)

    # Plotting before and after EMD:
    plots = [signal, imfs[0], imfs[1], imfs[2], imfs[3], imfs[4]]
    plot_title = ['Original Signal', 'IMF_1', 'IMF_2', 'IMF_3', 'IMF_4', 'IMF_5']
    if plot:
        zoom_1 = 1 if zoom_1 == 0 else zoom_1
        if zoom_1 and zoom_2:
            for i in range(6):
                plt.subplot(6, 1, i+1)
                pd.Series(plots[i][zoom_1:zoom_2]).plot(figsize=(15, 10), lw=0.5, title=plot_title[i])
                plt.tight_layout(pad=2.0)
        else:
            for i in range(6):
                plt.subplot(6, 1, i+1)
                pd.Series(plots[i]).plot(figsize=(15, 10), lw=0.5, title=plot_title[i])
                plt.tight_layout(pad=2.0)
        return plt.show()
    else:
        return signal, imfs


# Function for Wavelet-Denoising:
def wavelet_denoising(file_path, audio_index, sampling_rate='', plot=bool, zoom_1='', zoom_2=''):
    global signal, wl_signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    wl_signal = denoise_wavelet(signal, method='BayesShrink', mode='soft', wavelet_levels=5,
                                wavelet='sym8', rescale_sigma=True)

    # Plotting the signal before and after Wavelet-Denoising:
    if plot:
        zoom_1 = 1 if zoom_1 == 0 else zoom_1
        if zoom_1 and zoom_2:
            plt.subplot(2, 1, 1)
            pd.Series(signal[zoom_1:zoom_2]).plot(figsize=(15, 5), lw=0.5, title='Before Wavelet Denoising')
            plt.subplot(2, 1, 2)
            pd.Series(wl_signal[zoom_1:zoom_2]).plot(figsize=(15, 5), lw=0.5, title='After Wavelet Denoising')
        else:
            plt.subplot(2, 1, 1)
            pd.Series(signal).plot(figsize=(15, 5), lw=0.5, title='Before Wavelet Denoising')
            plt.subplot(2, 1, 2)
            pd.Series(wl_signal).plot(figsize=(15, 5), lw=0.5, title='After Wavelet Denoising')

        return plt.show()
    else:
        return signal, wl_signal


# Function for Digital Filter - Butterworth:
def digital_filter(file_path, audio_index, order, low_fc='', high_fc='', sampling_rate='',
                   plot=bool, zoom_1='', zoom_2=2):
    global signal, filtered_signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)

    if low_fc:
        low = float(low_fc) / (0.5 * float(sampling_rate))
    if high_fc:
        high = float(high_fc) / (0.5 * float(sampling_rate))

    # First condition - Lowpass Filter:
    if low_fc and not high_fc:
        b, a = scipy.signal.butter(order, low, 'lowpass', analog=False)
        filtered_signal = filtfilt(b, a, signal, axis=0)
    # Second Condition - Highpass Filter
    elif high_fc and not low_fc:  # Highpass Filter
        b, a = scipy.signal.butter(order, high, 'highpass', analog=False)
        filtered_signal = filtfilt(b, a, signal, axis=0)
    # Third Condition - Bandpass Filter:
    elif low_fc and high_fc:
        b, a = scipy.signal.butter(order, [high, low], 'bandpass', analog=False)
        filtered_signal = filtfilt(b, a, signal, axis=0)

    # Plotting the Time-Domain and Frequency-Domain (Spectrum) if plot argument is True:
    if plot:
        n_fft = 2048
        ft_signal = np.abs(librosa.stft(signal[:n_fft], hop_length=n_fft + 1))
        ft_filtered_signal = np.abs(librosa.stft(filtered_signal[:n_fft], hop_length=n_fft + 1))

        # Plotting the original signal and the filtered signal
        plots = [signal, filtered_signal, ft_signal, ft_filtered_signal]
        plot_title = ['Signal before filtering', 'Signal after filtering',
                      'Spectrum before filtering', 'Spectrum after filtering']

        zoom_1 = 1 if zoom_1 == 0 else zoom_1
        if zoom_1 and zoom_2:
            for i in range(2):
                plt.figure(figsize=(15, 10))
                plt.subplot(len(plots), 1, i+1)
                plt.plot(plots[i][zoom_1:zoom_2], lw=0.5)
                plt.title(plot_title[i])
            for i in range(2):
                plt.figure(figsize=(15, 10))
                plt.subplot(len(plots), 1, i+3)
                plt.plot(plots[i + 2], lw=0.5)
                plt.title(plot_title[i + 2])
            plt.tight_layout(pad=2.0)
        else:
            for i in range(4):
                plt.figure(figsize=(15, 10))
                plt.subplot(4, 1, i + 1)
                plt.plot(plots[i], lw=0.5)
                plt.title(plot_title[i])
            plt.tight_layout(pad=2.0)
        return plt.show()
    else:
        return signal, filtered_signal


# Combination of Signal Processing methods:
# Function for EMD + Wavelet-Denoising:
def emd_wavelet(file_path, audio_index, sampling_rate='', plot=bool, zoom_1='', zoom_2=''):
    global signal, imfs, emd_wavelet_signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    # Applying EMD method on the original signal
    emd = EMD()
    imfs = emd(signal)
    # Applying Wavelet-Denoising method to IMF_1 signal
    emd_wavelet_signal = denoise_wavelet(imfs[0], method='BayesShrink', mode='soft', wavelet_levels=5,
                                         wavelet='sym8', rescale_sigma=True)

    # Plotting the original signal, IMF_1 and IMF_1 after wavelet-denoising:
    if plot:
        zoom_1 = 1 if zoom_1 == 0 else zoom_1
        plots = [signal, imfs[0], emd_wavelet_signal]
        plot_title = ['Original Signal', 'IMF_1', 'Signal after applying IMF_1 and Wavelet-Denoising']

        if zoom_1 and zoom_2:
            for i in range(len(plots)):
                plt.figure(figsize=(15, 10))
                plt.subplot(len(plots), 1, i + 1)
                plt.plot(plots[i][zoom_1:zoom_2], lw=0.5)
                plt.title(plot_title[i])
        else:
            for i in range(len(plots)):
                plt.figure(figsize=(15, 10))
                plt.subplot(len(plots), 1, i + 1)
                plt.plot(plots[i], lw=0.5)
                plt.title(plot_title[i])
        return plt.show()
    else:
        return signal, imfs[0], emd_wavelet_signal


# Function for EMD + Digital-Filter:
def emd_dfilter(file_path, audio_index, order, low_fc='', high_fc='', sampling_rate='', plot=bool, zoom_1='',
                zoom_2=''):
    global signal, imfs, emd_dfilter_signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)
    # Applying EMD method on the original signal
    emd = EMD()
    imfs = emd(signal)

    # Applying Digital-Filter method to IMF_1 signal
    if low_fc:
        low = float(low_fc) / (0.5 * float(sampling_rate))
    if high_fc:
        high = float(high_fc) / (0.5 * float(sampling_rate))

    # First condition - Lowpass Filter:
    if low_fc and not high_fc:
        b, a = scipy.signal.butter(order, low, 'lowpass', analog=False)
        emd_dfilter_signal = filtfilt(b, a, imfs[0], axis=0)
    # Second Condition - Highpass Filter
    elif high_fc and not low_fc:  # Highpass Filter
        b, a = scipy.signal.butter(order, high, 'highpass', analog=False)
        emd_dfilter_signal = filtfilt(b, a, imfs[0], axis=0)
    # Third Condition - Bandpass Filter:
    elif low_fc and high_fc:
        b, a = scipy.signal.butter(order, [high, low], 'bandpass', analog=False)
        emd_dfilter_signal = filtfilt(b, a, imfs[0], axis=0)

    # Plotting the original signal, imf_1 and imf_1 after digital filter:
    if plot:
        zoom_1 = 1 if zoom_1 == 0 else zoom_1
        plots = [signal, imfs[0], emd_dfilter_signal]
        plot_title = ['Original Signal', 'IMF_1', 'Signal after applying IMF_1 and Digital-Filter']

        if zoom_1 and zoom_2:
            for i in range(len(plots)):
                plt.figure(figsize=(15, 10))
                plt.subplot(3, 1, i + 1)
                plt.plot(plots[i][zoom_1:zoom_2], lw=0.5)
                plt.title(plot_title[i])
        else:
            for i in range(len(plots)):
                plt.figure(figsize=(15, 10))
                plt.subplot(3, 1, i + 1)
                plt.plot(plots[i], lw=0.5)
                plt.title(plot_title[i])
        plt.tight_layout(pad=2.0)
        return plt.show()
    else:
        return signal, imfs[0], emd_dfilter_signal


# Function for EMD + Wavelet-Denoising + Digital-Filter
def emd_wl_dfilter(file_path, audio_index, order, low_fc='', high_fc='', sampling_rate='', plot=bool, zoom_1='',
                   zoom_2=''):
    global signal, imfs, emd_wavelet_signal, emd_wl_dfilter_signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)

    # Applying EMD method on the original signal
    emd = EMD()
    imfs = emd(signal)

    # Applying Wavelet-Denoising method to IMF_1 signal
    emd_wavelet_signal = denoise_wavelet(imfs[0], method='BayesShrink', mode='soft', wavelet_levels=5,
                                         wavelet='sym8', rescale_sigma=True)

    # Applying Digital-Filter method to emd_wavelet_signal
    if low_fc:
        low = float(low_fc) / (0.5 * float(sampling_rate))
    if high_fc:
        high = float(high_fc) / (0.5 * float(sampling_rate))

    # First condition - Lowpass Filter:
    if low_fc and not high_fc:
        b, a = scipy.signal.butter(order, low, 'lowpass', analog=False)
        emd_wl_dfilter_signal = filtfilt(b, a, emd_wavelet_signal, axis=0)
    # Second Condition - Highpass Filter
    elif high_fc and not low_fc:  # Highpass Filter
        b, a = scipy.signal.butter(order, high, 'highpass', analog=False)
        emd_wl_dfilter_signal = filtfilt(b, a, emd_wavelet_signal, axis=0)
    # Third Condition - Bandpass Filter:
    elif low_fc and high_fc:
        b, a = scipy.signal.butter(order, [high, low], 'bandpass', analog=False)
        emd_wl_dfilter_signal = filtfilt(b, a, emd_wavelet_signal, axis=0)

    # plotting the original signal, emd_wavelet signal and emd_wl_dfilter:
    if plot:
        zoom_1 = 1 if zoom_1 == 0 else zoom_1
        plots = [signal, imfs[0], emd_wavelet_signal, emd_wl_dfilter_signal]
        plot_title = ['Original Signal', 'IMF_1', 'Signal after applying IMF_1 and Wavelet-Denoising',
                      'Signal after applying IMF_1, Wavelet-Denoising and Digital-Filter']
        if zoom_1 and zoom_2:
            for i in range(len(plots)):
                plt.figure(figsize=(15, 10))
                plt.subplot(len(plots), 1, i + 1)
                plt.plot(plots[i][zoom_1:zoom_2], lw=0.5)
                plt.title(plot_title[i])
        else:
            for i in range(len(plots)):
                plt.figure(figsize=(15, 10))
                plt.subplot(len(plots), 1, i + 1)
                plt.plot(plots[i], lw=0.5)
                plt.title(plot_title[i])
        plt.tight_layout(pad=2.0)
        return plt.show()
    else:
        return signal, imfs[0], emd_wavelet_signal, emd_wl_dfilter_signal


# 3. FEATURE EXTRACTION:
# Function to extract the Spectrogram:
def spectrogram(file_path, audio_index, sampling_rate=''):
    global signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)

    # Extracting the Spectrogram:
    signal_stft = librosa.stft(signal)
    signal_stft_db = librosa.amplitude_to_db(np.abs(signal_stft), ref=np.max)
    print(signal_stft_db.shape)

    # Plotting the transformed audio file:
    fig, ax = plt.subplots(figsize=(10, 5))
    img = librosa.display.specshow(signal_stft_db, x_axis='time', y_axis='log', ax=ax)
    ax.set_title('Spectrogram', fontsize=20)
    fig.colorbar(img, ax=ax, format=f'%0.2f')
    return plt.show()


# Function to extract the Mel Spectrogram:
def mel_spectrogram(file_path, audio_index, sampling_rate=''):
    global signal
    signal, sr = librosa.load(file_path[audio_index], sr=sampling_rate)

    # Extracting the Mel-Spectrogram:
    signal_mel = librosa.feature.melspectrogram(signal, sr=sr, n_mels=128 * 2)
    signal_mel_db = librosa.amplitude_to_db(signal_mel, ref=np.max)
    print(signal_mel_db.shape)

    # Plotting the Mel-Spectrogram:
    fig, ax = plt.subplots(figsize=(10, 5))
    img = librosa.display.specshow(signal_mel_db, x_axis='time', y_axis='log', ax=ax)
    ax.set_title('Mel Spectrogram', fontsize=20)
    fig.colorbar(img, ax=ax, format=f'%0.2f')
    return plt.show()

# Wavelet-Transform:
